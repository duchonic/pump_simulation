# pump test

## endurence test for pumps/flowmeter

<b>Pump Simulation</b> This is an endurance test for pumps. A robot is programmed to test a pump. 
The pump is connected to a flowmeter that outputs a Pulse Width Modulation (PWM) signal when water flow is detected.
<b>Starting the Pump</b> The robot will set a digital input on the pump to HIGH to start the pump.
<b>Checking the Flow</b> The robot will record the flow detected with the help of the PWM signal.

### robo layout

```mermaid
graph TD;
  roboter --data--> computer
  PUMP <--digital_IO--> roboter
  FLOW_METER --PWM--> roboter 
  computer --settings--> roboter
```

### robo states

```mermaid
graph TD;
  IDLE--start-->PUMP_ON;
  PUMP_ON--flow_detected-->RECORD_FLOW;
  RECORD_FLOW--volume_reched-->PUMP_OFF;
  PUMP_OFF-->IDLE;
```



## systemtest for pump software

<b>Robot as Pump/Flow Dummy</b>. 
A robot should function as a pump/flow dummy. 
The robot is connected to the pump hardware and sends a Pulse Width Modulation (PWM) signal if a digital pump signal is received.
<b>Starting the PWM Signal</b> The PWM signal is initiated when the digital pump-on signal is set.

### robo layout

```mermaid
graph TD;
application --pump--> pump_driver
pump_driver --volume--> application 
pump_driver --digital_IO--> roboter
roboter --PWM--> pump_driver
roboter --data-->computer
computer --settings--> roboter
```

### robo states

```mermaid
graph TD;
  IDLE--digita_IO-->PUMP_on_PWM_on
  PUMP_on_PWM_on--timeout-->IDLE
```


## realworld


This is just a simplification of our setup. 
There are several additional sensors in our setup like level sensors, tank detection, pressure sensors, and additional actuators.

Here are two ideas about our implementation

### solution A

Control both robots with a controllino. Arduino / C++ or python.

https://www.controllino.com/

### solution B

Control both robots with python / javascript scripts and yoctopuce sensors/actors.

Use the PWM Rx device from yoctopuce. 
The Yocto-PWM-Rx is a 2 channel PWM input. 
Each channel can report frequency, duty cycle, and pulse length. 
It works from 0.05Hz to 250Khz and accepts any -30..+30V signal as long as it crosses the 0.7V threshold.

https://www.yoctopuce.com/EN/article/counting-pulses

Also the PWM Tx for the systemtest setup.

https://www.yoctopuce.com/EN/article/the-yocto-maxi-io-v2-and-the-yocto-io

### solution C

viam

https://discord.com/channels/1083489952408539288/1085305660310311044/threads/1232254654189010974